require 'oauth2'

API_UID = "58a5e76c6c372daeececf349137e64bcdcf2ce0524de91530306f1c713789637"
API_SECRET = "49359cc3513122ffcab960c412b6e4f115645ba9d74a5a7228a0540109ab3626"
AUTH_ENDPOINT = "https://api.intra.42.fr"
IMG_PATH = "/Users/tolsadus/Work/api42/photos/"

LOGIN = ARGV[0]

puts LOGIN

client = OAuth2::Client.new(API_UID, API_SECRET, site: AUTH_ENDPOINT) do |stack|
    stack.request :multipart
    stack.request :url_encoded
    stack.adapter  Faraday.default_adapter
end

token = client.client_credentials.get_token

image = File.open("#{IMG_PATH}#{LOGIN}.jpg")
p image.size
upload = Faraday::UploadIO.new(image, "image/#{image.path.split('.').last}", "image.#{image.path.split('.').last}")

response = token.put("/v2/users/#{LOGIN}", body: {user: {image: upload}})
p response.status
puts "Upload for #{LOGIN} complete"
